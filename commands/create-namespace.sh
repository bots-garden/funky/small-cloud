#!/bin/sh
NAMESPACE=${1}
export KUBECONFIG=../cluster/k3s.yaml
kubectl create namespace ${NAMESPACE}