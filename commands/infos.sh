#!/bin/sh
export KUBECONFIG=../cluster/k3s.yaml
kubectl cluster-info
kubectl get nodes -o wide
kubectl top node
kubectl top pod -n kube-system

