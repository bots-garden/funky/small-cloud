# Tools

## setup

```bash
./deploy.sh
```

The 2 volume pods access to the same volume mount (on `/data`)

> Remark: The volume mount is reachable only in the namespace.
