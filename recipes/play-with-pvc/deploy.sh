#!/bin/sh
export KUBECONFIG=../../cluster/k3s.yaml

#export SUB_DOMAIN="172.16.245.122.nip.io"
NAMESPACE="tools"
kubectl describe namespace ${NAMESPACE} 
exit_code=$?
if [[ exit_code -eq 1 ]]; then
  echo "🖐️ ${NAMESPACE} does not exist"
  echo "⏳ Creating the namespace..."
  kubectl create namespace ${NAMESPACE}
else 
  echo "👋 ${NAMESPACE} already exists"
fi

kubectl create -f pvc.yaml -n ${NAMESPACE}
kubectl create -f pod1.yaml -n ${NAMESPACE}
kubectl create -f pod2.yaml -n ${NAMESPACE}
