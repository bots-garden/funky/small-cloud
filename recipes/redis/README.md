# Redis

## Connect from the same namespace

```bash
kubectl exec -n databases -it redis-client -- /bin/bash
redis-cli -h redis-master
set firstname bob
set lastname morane
get firstname
get lastname
```

## Connect from a different namespace

```bash
# use the name of the redis namespace after the `.` (dot) 
redis-cli -h redis-master.databases
```
