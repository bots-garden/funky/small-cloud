#!/bin/sh
export KUBECONFIG=../../cluster/k3s.yaml
NAMESPACE="databases"
kubectl port-forward --namespace ${NAMESPACE} svc/redis-master 6379:6379 &