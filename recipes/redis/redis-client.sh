#!/bin/sh
export KUBECONFIG=../../cluster/k3s.yaml

NAMESPACE="databases"
kubectl exec -n ${NAMESPACE} -it redis-client -- /bin/bash